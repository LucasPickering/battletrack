from btcore import util

from .common import BtTestCase, vcr


class MatchTests(BtTestCase):

    MATCHES = {
        # API version 7.5.0
        # PC ranked Squad FPP
        'adcd8bfa-10b3-468d-8718-4915f76f5dbd': {
            'platform': 'steam',
            'shard': 'pc-eu',
            'mode': 'squad',
            'perspective': 'fpp',
            'map': {'name': 'Savage_Main', 'size': 4000},
            'date': '2018-11-15T16:02:20-05:00',
            'duration': 1265,
            'custom_match': False,
        },
        # PC ranked squad TPP
        'a9b2b457-04c6-4241-95e9-b0eae4e123c2': {
            'platform': 'steam',
            'shard': 'pc-eu',
            'mode': 'squad',
            'perspective': 'tpp',
            'map': {'name': 'Savage_Main', 'size': 4000},
            'date': '2018-11-17T16:40:49-05:00',
            'duration': 1374,
            'custom_match': False,
        },
        # PC custom Squad FPP
        'e3a59ec5-0b15-4ff1-9e28-40bcaa2e34b7': {
            'platform': 'steam',
            'shard': 'pc-eu',
            'mode': 'squad',
            'perspective': 'fpp',
            'map': {'name': 'Erangel_Main', 'size': 8000},
            'date': '2018-11-15T14:41:31-05:00',
            'duration': 1700,
            'custom_match': True,
        },
        # Xbox ranked squad TPP
        'b2e31bf9-c7fc-41f0-bc79-552986ffa88b': {
            'platform': 'xbox',
            'shard': 'xbox-na',
            'mode': 'squad',
            'perspective': 'tpp',
            'map': {'name': 'Savage_Main', 'size': 4000},
            'date': '2018-11-18T13:19:54-05:00',
            'duration': 1395,
            'custom_match': False,
        },
    }

    @vcr.use_cassette('match.yml')
    def get_match(self, id):
        return self.get(f'/api/core/matches/{id}')

    def setUp(self):
        super().setUp()
        self.matches = {match_id: self.get_match(
            match_id) for match_id in self.MATCHES.keys()}

    def test_get_match(self):
        for match_id, match in self.matches.items():
            self.check_dict(match, id=match_id, **self.MATCHES[match_id])

    def test_rosters_sorted(self):
        # Make sure all rosters are sorted by placement
        for match in self.matches.values():
            self.check_sorted(match['rosters'], key=lambda m: m['win_place'])


class PlayerTests(BtTestCase):

    PLATFORM = 'steam'
    PLAYER_NAME = 'BreaK'
    CASSETTE_FILE = 'player.yml'

    @vcr.use_cassette(CASSETTE_FILE)
    def get_player(self, platform, key, pop_matches=True):
        params = 'popMatches' if pop_matches else ''
        return self.get(f'/api/core/players/{platform}/{key}?{params}')

    def test_get_player(self):
        player = self.get_player(platform=self.PLATFORM, key=self.PLAYER_NAME)
        self.check_dict(player, name=self.PLAYER_NAME)

        # Make sure getting by ID returns the same thing
        self.assertEqual(player, self.get_player(
            platform=self.PLATFORM, key=player['id']))

    def test_player_match(self):
        player = self.get_player(platform=self.PLATFORM, key=self.PLAYER_NAME)
        matches = player['matches']
        self.assertEqual(3, len(matches))
        match = matches[0]

        self.check_dict(
            match,
            match_id='2485b144-85a7-46e9-baa9-0151271735d2',
            roster=[
                {'player_id': 'account.a36bed11ed214557b0ddef9ef1a56d07',
                 'player_name': 'BreaK'},
                {'player_id': 'account.a900b528396940e29e0151435ff317e5',
                 'player_name': 'Liquid_ibiza'},
                {'player_id': 'account.69740cb0aed84b38ad6529d2a3ce847e',
                 'player_name': 'TwitchTV_Lumi'},
            ],
        )
        self.check_dict(
            match['summary'],
            shard='pc-eu',
            mode='squad',
            perspective='fpp',
            map_name='Erangel_Main',
            date='2018-11-18T13:20:24-05:00',
            duration=1848,
            custom_match=False,
            roster_count=30,
        )
        self.check_dict(
            match['stats'],
            assists=0,
            boosts=4,
            damage_dealt=320.728821,
            dbnos=4,
            death_type='byplayer',
            headshot_kills=2,
            heals=2,
            kill_place=2,
            kill_streaks=3,
            kills=6,
            longest_kill=38.20639,
            most_damage=0,
            revives=1,
            ride_distance=1920.018,
            swim_distance=0.0,
            road_kills=0,
            team_kills=0,
            time_survived=735.031,
            vehicle_destroys=0,
            walk_distance=1195.02063,
            weapons_acquired=5,
            win_place=16,
        )

    def test_matches_sorted(self):
        # Make sure all matches are sorted by date
        player = self.get_player(platform=self.PLATFORM, key=self.PLAYER_NAME)
        self.check_sorted(
            player['matches'], key=lambda m: m['summary']['date'], reverse=True)

    def test_pop_matches(self):
        # Don't populate matches, all summaries are None
        matches = self.get_player(
            self.PLATFORM, self.PLAYER_NAME, pop_matches=False)['matches']
        self.assertEqual(3, len(matches))
        self.assertTrue(all(not m['summary'] for m in matches))

        # Populate matches, all summaries should be populated
        matches = self.get_player(
            self.PLATFORM, self.PLAYER_NAME, pop_matches=True)['matches']
        self.assertEqual(3, len(matches))
        self.assertTrue(all(m['summary'] for m in matches))


class PlatformsTests(BtTestCase):
    def test_platforms(self):
        platforms = self.client.get('/api/core/platforms').json()
        self.assertEqual(util.PLATFORMS, platforms)
