from django.test import TestCase

from btcore import util


class ParseModePerspectiveTestCase(TestCase):
    def test_mode_only(self):
        mode, perspective = util.parse_mode_perspective('squad')
        self.assertEqual(mode, 'squad')
        self.assertEqual(perspective, 'tpp')

    def test_mode_and_perspective(self):
        mode, perspective = util.parse_mode_perspective('squad-fpp')
        self.assertEqual(mode, 'squad')
        self.assertEqual(perspective, 'fpp')

    def test_custom_fpp_match(self):
        mode, perspective = util.parse_mode_perspective('normal-squad-fpp')
        self.assertEqual(mode, 'squad')
        self.assertEqual(perspective, 'fpp')

    # Need:
    # - Custom TPP
    # - Weekly special
