import itertools
import logging
import re
import time

from django.conf import settings

MAP_SIZES = {
    'Erangel_Main': 8000,
    'Desert_Main': 8000,
    'Savage_Main': 4000,
    'Range_Main': 2000,
}
PLATFORMS = [
    'steam',
    'xbox',
    'kakao',
]

MATCH_ID_LENGTH = 36
PLAYER_ID_LENGTH = 40
ROSTER_ID_LENGTH = 36


MODE_PERSPECTIVE_REGEXES = [
    # e.g. "squad"
    re.compile(r'^(?P<mode>[\w\d]+)$'),
    # e.g. "squad-fpp"
    re.compile(r'(?P<mode>[\w\d]+)-(?P<perspective>[ft]pp)'),
    # e.g. "normal-squad-fpp"
    re.compile(r'^[\w\d]+-(?P<mode>[\w\d]+)-(?P<perspective>[ft]pp)$'),
]

_logger = logging.getLogger(settings.BT_LOGGER_NAME)


def choices(it):
    return ((e, e) for e in it)


def shard_to_platform(shard):
    if shard == 'pc-kakao':
        return 'kakao'
    if shard.startswith('pc-'):
        return 'steam'
    if shard.startswith('xbox-'):
        return 'xbox'
    _logger.error(f"Unknown shard: {shard}")
    return ''


def parse_mode_perspective(game_mode):
    # Test multiple regexes against the game mode
    # This is cleaner than having one nasty regex
    for rgx in MODE_PERSPECTIVE_REGEXES:
        m = rgx.match(game_mode)
        if m:
            groups = m.groupdict()
            # Default perspective to 'tpp'
            return groups['mode'], groups.get('perspective', 'tpp')
    # None of the regexes matched, this is bad
    _logger.error(f"Unknown game mode: {game_mode}")
    return '', ''


def timed(func):
    start = time.time()
    rv = func()
    elapsed = time.time() - start
    return (rv, elapsed)


def timed_dec(func):
    def wrapper(*args, **kwargs):
        rv, elapsed = timed(lambda: func(*args, **kwargs))  # Run with a timer

        args_strs = (str(arg) for arg in args)
        kwargs_strs = (f'{k}={v}' for k, v in kwargs.items())
        arg_str = ','.join(itertools.chain(args_strs, kwargs_strs))
        _logger.debug(f"{func.__name__}({arg_str}) took {elapsed:.4f}s")
        return rv

    # Only run the timing wrapper if debug mode is enabled
    return wrapper if settings.DEBUG else func


class Timed:
    def __init__(self, msg="Took {time}"):
        self._msg = msg

    def __enter__(self):
        self._start = time.time()

    def __exit__(self, exc_type, exc_value, traceback):
        elapsed = time.time() - self._start
        _logger.debug(self._msg.format(time=f"{elapsed:.4f}s"))
