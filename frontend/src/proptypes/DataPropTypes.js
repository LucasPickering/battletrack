import PropTypes from 'prop-types';

import MapPropTypes from './MapPropTypes';

const exported = {};

exported.playerMatchStats = PropTypes.shape({
  // This doesn't cover all fields - only the ones we use
  damage_dealt: PropTypes.number.isRequired,
  kills: PropTypes.number.isRequired,
  ride_distance: PropTypes.number.isRequired,
  swim_distance: PropTypes.number.isRequired,
  time_survived: PropTypes.number.isRequired,
  walk_distance: PropTypes.number.isRequired,
  win_place: PropTypes.number.isRequired,
});

exported.playerSummary = PropTypes.shape({
  player_id: PropTypes.string.isRequired,
  player_name: PropTypes.string.isRequired,
  stats: exported.playerMatchStats, // Only appears in some endpoints
});

exported.map = PropTypes.shape({
  name: PropTypes.string.isRequired,
  size: PropTypes.number.isRequired,
});

exported.matchRoster = PropTypes.shape({
  id: PropTypes.string.isRequired,
  win_place: PropTypes.number.isRequired,
  players: PropTypes.arrayOf(exported.playerSummary.isRequired).isRequired,
});

exported.match = PropTypes.shape({
  id: PropTypes.string.isRequired,
  platform: PropTypes.string.isRequired,
  mode: PropTypes.string.isRequired,
  perspective: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  duration: PropTypes.number.isRequired,
  custom_match: PropTypes.bool.isRequired,
  map: exported.map.isRequired,
  rosters: PropTypes.arrayOf(exported.matchRoster.isRequired),
});

exported.matchSummary = PropTypes.shape({
  id: PropTypes.string.isRequired,
  platform: PropTypes.string.isRequired,
  mode: PropTypes.string.isRequired,
  perspective: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  duration: PropTypes.number.isRequired,
  custom_match: PropTypes.bool.isRequired,
  map_name: PropTypes.string.isRequired,
  roster_count: PropTypes.number.isRequired,
});

exported.playerMatch = PropTypes.shape({
  summary: exported.matchSummary.isRequired,
  roster: PropTypes.arrayOf(exported.playerSummary.isRequired).isRequired,
  stats: exported.playerMatchStats.isRequired,
});

exported.rosterMatch = PropTypes.shape({
  id: PropTypes.string.isRequired,
  win_place: PropTypes.number.isRequired,
  players: PropTypes.arrayOf(
    PropTypes.shape({
      player_id: PropTypes.string.isRequired,
      player_name: PropTypes.string.isRequired,
      stats: exported.playerMatchStats.isRequired,
    }).isRequired
  ).isRequired,
});

exported.player = PropTypes.shape({
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  matches: PropTypes.arrayOf(exported.playerMatch).isRequired,
});

exported.telemetry = PropTypes.shape({
  plane: MapPropTypes.ray.isRequired,
  zones: PropTypes.arrayOf(MapPropTypes.circle.isRequired).isRequired,
  events: PropTypes.objectOf(PropTypes.array).isRequired,
});

export default exported;
