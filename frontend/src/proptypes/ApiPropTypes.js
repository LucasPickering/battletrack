import PropTypes from 'prop-types';

const exported = {};

exported.error = PropTypes.shape({
  status: PropTypes.number.isRequired,
});

exported.apiState = PropTypes.shape({
  params: PropTypes.object,
  loading: PropTypes.bool.isRequired,
  data: PropTypes.any,
  error: PropTypes.error,
});

export default exported;
