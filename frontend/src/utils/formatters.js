import { capitalize } from 'lodash';
import moment from 'moment-timezone';

import Localization from 'utils/Localization';

export function formatGameMode(gameMode) {
  return gameMode ? capitalize(gameMode) : 'Other';
}

export function formatPerspective(perspective) {
  return perspective ? perspective.toUpperCase() : 'Other';
}

export function formatPlaylist(gameMode, perspective) {
  return `${formatGameMode(gameMode)} ${formatPerspective(perspective)}`;
}

export function formatDate(date, format = 'MMMM D, YYYY, HH:mm') {
  return moment(date).format(format);
}

export function formatSeconds(seconds, format = 'm[m] ss[s]') {
  return moment.utc(seconds * 1000).format(format);
}

export function formatMapName(mapName) {
  return Localization.maps[mapName];
}

export function formatDamage(damage) {
  return damage.toFixed(0);
}

export function formatDistance(distance) {
  return `${distance.toFixed(2)} km`;
}

export function formatPlatform(platform) {
  return capitalize(platform);
}

export function formatItem(item) {
  const { name, stack_count: stackCount } = item;
  return `${stackCount}x ${name}`;
}
