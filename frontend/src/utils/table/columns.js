import {
  formatDate,
  formatGameMode,
  formatMapName,
  formatPerspective,
  formatPlatform,
} from 'utils/formatters';
import { makeLabels } from 'utils/funcs';
import FilterSelect from 'components/core/FilterSelect';
import IconLink from 'components/core/IconLink';
import WinPlacement from 'components/common/WinPlacement';
import makeCellComponent from 'hoc/makeCellComponent';
import makeTableFilter from 'hoc/makeTableFilter';

const MAPS = Object.freeze(
  makeLabels(['Erangel_Main', 'Desert_Main', 'Savage_Main'], formatMapName)
);
const GAME_MODES = Object.freeze(
  makeLabels(['solo', 'duo', 'squad', 'custom'], formatGameMode)
);

const PERSPECTIVES = Object.freeze(
  makeLabels(['fpp', 'tpp'], formatPerspective)
);

function makeColumnBuilder(cell) {
  return props => ({
    ...cell,
    ...props,
  });
}

/**
 * Extra column fields:
 * rosterCountGetter: Function to convert original row value into roster count
 */
export const winPlaceColumn = makeColumnBuilder({
  filterable: false,
  width: 112,
  Cell: makeCellComponent(WinPlacement, ({ column, value, original }) => ({
    winPlace: value,
    rosterCount: column.rosterCountGetter
      ? column.rosterCountGetter(original)
      : null,
  })),
});

export const mapColumn = makeColumnBuilder({
  width: 100,
  Header: 'Map',
  Cell: ({ value }) => formatMapName(value),
  Filter: makeTableFilter(FilterSelect, {
    options: MAPS,
  }),
});

export const gameModeColumn = makeColumnBuilder({
  width: 100,
  Header: 'Game Mode',
  Cell: ({ value }) => formatGameMode(value),
  Filter: makeTableFilter(FilterSelect, {
    options: GAME_MODES,
  }),
});

export const perspectiveColumn = makeColumnBuilder({
  width: 80,
  Header: 'Persp.',
  Cell: ({ value }) => formatPerspective(value),
  Filter: makeTableFilter(FilterSelect, {
    options: PERSPECTIVES,
  }),
});

export const platformColumn = makeColumnBuilder({
  Header: 'Platform',
  Cell: ({ value }) => formatPlatform(value),
});

export const dateColumn = makeColumnBuilder({
  width: 160,
  filterable: false,
  Header: 'Date',
  Cell: ({ value }) => formatDate(value, 'MMM DD, YYYY HH:mm'),
});

/**
 * Extra column fields:
 * linkBuilder: function to convert cell value into link
 */
export const linkColumn = makeColumnBuilder({
  width: 24,
  filterable: false,
  sortable: false,
  Cell: makeCellComponent(IconLink, ({ column, value }) => ({
    to: column.linkBuilder(value),
  })),
});
