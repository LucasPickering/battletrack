import { noop } from 'lodash';
import React from 'react';

export default React.createContext({
  params: null,
  loading: false,
  data: null,
  error: null,
  getUrl: noop,
});
