import { noop } from 'lodash';
import React from 'react';

const OverviewContext = React.createContext({
  matchId: null,
  initialized: false,
  rosterPalette: null,
  enabledFilters: null,
  timeRange: null,
  setEnabledFilters: noop,
  setTimeRange: noop,
});

export default OverviewContext;
