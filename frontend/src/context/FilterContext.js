import { noop } from 'lodash';
import React from 'react';

export default React.createContext({
  filterVals: {},
  setFilterVal: noop,
});
