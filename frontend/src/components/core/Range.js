import 'rc-slider/assets/index.css';
import PropTypes from 'prop-types';
import React from 'react';
import Slider from 'rc-slider';
import classnames from 'classnames';

import classes from './Range.module.scss';

const TooltipRange = Slider.createSliderWithTooltip(Slider.Range); // Janky AF

const Range = ({ className, ...rest }) => (
  <TooltipRange className={classnames(classes.range, className)} {...rest} />
);

Range.propTypes = {
  className: PropTypes.string,
  count: PropTypes.number,
};

Range.defaultProps = {
  className: null,
  count: 1,
};

export default Range;
