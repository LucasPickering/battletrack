import { noop } from 'lodash';
import PropTypes from 'prop-types';
import React from 'react';

const ALL_KEY = '__all__';

const FilterSelect = ({ selected, options, onChange }) => (
  <select value={selected} onChange={e => onChange(e.target.value)}>
    <option value={ALL_KEY}>All</option>
    {options.map(({ key, label }) => (
      <option key={key} value={key}>
        {label}
      </option>
    ))}
  </select>
);

const keyType = PropTypes.oneOfType([
  PropTypes.string.isRequired,
  PropTypes.number.isRequired,
]);

FilterSelect.propTypes = {
  options: PropTypes.arrayOf(
    PropTypes.shape({
      key: keyType.isRequired,
      label: PropTypes.string.isRequired,
    }).isRequired
  ),
  selected: keyType,
  onChange: PropTypes.func,
};

FilterSelect.defaultProps = {
  selected: ALL_KEY,
  options: [],
  onChange: noop,
};

export default FilterSelect;
