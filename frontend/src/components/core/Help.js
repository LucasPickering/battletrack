import { OverlayTrigger, Tooltip } from 'react-bootstrap';
import PropTypes from 'prop-types';
import React from 'react';

import Icon from './Icon';

const Help = ({ className, id, children, ...rest }) => (
  <OverlayTrigger
    overlay={<Tooltip id={id}>{children}</Tooltip>}
    placement="top"
  >
    <Icon
      className={className}
      name="question-circle"
      format="regular"
      {...rest}
    />
  </OverlayTrigger>
);

Help.propTypes = {
  className: PropTypes.string,
  id: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
};

Help.defaultProps = {
  className: null,
};

export default Help;
