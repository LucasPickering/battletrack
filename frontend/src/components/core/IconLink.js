import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import React from 'react';

import Icon from './Icon';

const IconLink = ({ icon, ...rest }) => (
  <Link {...rest}>
    <Icon name={icon} />
  </Link>
);

IconLink.propTypes = {
  icon: PropTypes.string,
};
IconLink.defaultProps = {
  icon: 'link',
};

export default IconLink;
