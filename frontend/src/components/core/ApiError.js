import PropTypes from 'prop-types';
import React from 'react';

import ApiPropTypes from 'proptypes/ApiPropTypes';

import classes from './ApiError.module.scss';

const ErrorComponent = ({ error }) => (
  <div className={classes.error}>
    <h2>Error</h2>
    {error.status === 404 ? 'Not Found' : error.status}
  </div>
);

ErrorComponent.propTypes = {
  error: ApiPropTypes.error.isRequired,
};

const ApiError = ({ error, errors }) => {
  if (error) {
    return <ErrorComponent error={error} />;
  }
  if (errors) {
    return Object.entries(errors).map(([key, e]) => (
      <ErrorComponent key={key} error={e} />
    ));
  }
  return null;
};

ApiError.propTypes = {
  error: ApiPropTypes.error,
  errors: PropTypes.objectOf(ApiPropTypes.error.isRequired),
};

ApiError.defaultProps = {
  error: null,
  errors: null,
};

export default ApiError;
