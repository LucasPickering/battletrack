import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';

import CommonPropTypes from 'proptypes/CommonPropTypes';

import classes from './Button.module.scss';

const Button = ({ className, background, children, ...rest }) => (
  <button
    className={classNames(
      classes.button,
      background ? classes.background : classes.noBackground,
      className
    )}
    type="button"
    {...rest}
  >
    {children}
  </button>
);

Button.propTypes = {
  className: PropTypes.string,
  background: PropTypes.bool,
  children: CommonPropTypes.children,
  text: PropTypes.string,
  icon: PropTypes.element,
};

Button.defaultProps = {
  className: null,
  background: true,
  children: null,
  text: '',
  icon: null,
};

export default Button;
