import 'react-table/react-table.css';
import React from 'react';
import ReactTable from 'react-table';
import classNames from 'classnames';

import classes from './DataTable.module.scss';

const FILTER_ALL_KEY = '__all__';

const FILTER_METHOD = (filter, row) => {
  const id = filter.pivotId || filter.id;
  const filterVal = filter.value;
  const cellVal = row[id];
  return filterVal === FILTER_ALL_KEY || cellVal === filterVal;
};

/**
 * Small wrapper around the ReactTable component
 * @param {*} props props to pass to the ReactTable
 */
const DataTable = ({ className, ...rest }) => (
  <ReactTable
    className={classNames(
      // The linter is too dumb to notice that this class is real
      // eslint-disable-next-line css-modules/no-undef-class
      classes.dataTable,
      '-striped',
      '-highlight',
      className
    )}
    {...rest}
  />
);

DataTable.propTypes = ReactTable.propTypes;

// Override defaults for table props
DataTable.defaultProps = {
  className: null,
  resizable: false,
  pageSizeOptions: [10, 25, 50],
  defaultPageSize: 10,
  minRows: 1,
  defaultFilterMethod: FILTER_METHOD,
};

export default DataTable;
