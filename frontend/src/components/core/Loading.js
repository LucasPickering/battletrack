import { ClipLoader } from 'react-spinners';
import PropTypes from 'prop-types';
import React from 'react';

import CommonPropTypes from 'proptypes/CommonPropTypes';

import classes from './Loading.module.scss';

const Loading = ({ children, loading, loadingComp, text, ...rest }) =>
  loading ? (
    <div className={classes.loading}>
      {React.createElement(loadingComp, {
        loading: true,
        color: '$highlight-color-1',
        ...rest,
      })}
      {text && <p className={classes.loadingText}>{text}</p>}
      {children}
    </div>
  ) : null;

Loading.propTypes = {
  children: CommonPropTypes.children,
  loading: PropTypes.bool,
  loadingComp: PropTypes.func,
  text: PropTypes.string,
  size: PropTypes.number,
};

Loading.defaultProps = {
  children: null,
  loading: true,
  loadingComp: ClipLoader,
  text: null,
  size: 100,
};

export default Loading;
