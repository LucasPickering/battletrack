import { isEqual } from 'lodash';
import React from 'react';

class LeafletComponent extends React.Component {
  shouldComponentUpdate(nextProps, nextState) {
    return !isEqual(this.props, nextProps) || !isEqual(this.state, nextState);
  }
}

export default LeafletComponent;
