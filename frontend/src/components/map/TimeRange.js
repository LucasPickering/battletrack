import { range } from 'lodash';
import PropTypes from 'prop-types';
import React, { useEffect, useMemo } from 'react';
import classNames from 'classnames';

import { formatSeconds } from 'utils/formatters';
import MapPropTypes from 'proptypes/MapPropTypes';
import Range from 'components/core/Range';

import classes from './TimeRange.module.scss';

/**
 * Builds the list of marks to be displayed on the slider.
 */
function getMarks(maxTime) {
  // Build an object of marks to put on the slider, at regular time intervals
  const marks = {};
  range(0, maxTime, 5 * 60).forEach(i => {
    marks[i] = formatSeconds(i, 'm[m]');
  });

  // Add the final mark
  marks[maxTime] = formatSeconds(maxTime, 'm[m]');
  return marks;
}

const TimeRange = ({ className, maxTime, timeRange, setTimeRange }) => {
  // Memoized hook for computing the marks on the slider
  const marks = useMemo(() => getMarks(maxTime), [maxTime]);

  // Effect hook to reset the time range when the max time changes
  useEffect(
    () => setTimeRange([0, maxTime]), // Reset to the full time range
    [maxTime]
  );

  return (
    <Range
      className={classNames(classes.timeRange, className)}
      count={1}
      max={maxTime}
      value={timeRange}
      onChange={setTimeRange}
      marks={marks}
      tipFormatter={formatSeconds}
    />
  );
};

TimeRange.propTypes = {
  className: PropTypes.string,
  maxTime: PropTypes.number.isRequired,
  timeRange: MapPropTypes.timeRange.isRequired, // [min, max]
  setTimeRange: PropTypes.func.isRequired,
};

TimeRange.defaultProps = {
  className: null,
};

export default TimeRange;
