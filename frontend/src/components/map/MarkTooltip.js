import { Popup } from 'react-leaflet';
import PropTypes from 'prop-types';
import React from 'react';
import uniqid from 'uniqid';

import { formatSeconds } from 'utils/formatters';
import Icon from 'components/core/Icon';
import MapPropTypes from 'proptypes/MapPropTypes';

import classes from './MarkTooltip.module.scss';

const MarkTooltip = props => {
  const { title, time, children } = props;

  const content = [
    { icon: { name: 'clock' }, text: formatSeconds(time) }, // Add event time
    ...children,
  ];

  return (
    <Popup className={classes.tooltip}>
      <h4 className={classes.title}>{title}</h4>
      {content.map(({ icon, text }) => (
        <div className={classes.tooltipLine} key={uniqid()}>
          {icon && <Icon {...icon} />}
          <p>{text}</p>
        </div>
      ))}
    </Popup>
  );
};

MarkTooltip.propTypes = {
  title: PropTypes.string.isRequired,
  time: PropTypes.number.isRequired,
  children: MapPropTypes.tooltipContent,
};

MarkTooltip.defaultProps = {
  children: [],
};

export default MarkTooltip;
