import { Marker } from 'react-leaflet';
import { noop } from 'lodash';
import Leaflet from 'leaflet';
import PropTypes from 'prop-types';
import React from 'react';
import reactToCss from 'react-style-object-to-css';

import { buildIconProps } from 'components/core/Icon';
import { toLeaflet } from 'utils/funcs';
import Localization from 'utils/Localization';
import MapPropTypes from 'proptypes/MapPropTypes';

import './EventMark.module.scss';
import LeafletComponent from './LeafletComponent';
import MarkTooltip from './MarkTooltip';

class EventMark extends LeafletComponent {
  render() {
    const {
      type,
      icon,
      time,
      tooltip,
      pos,
      player,
      rosterPalette,
      onMarkSelect,
      ...rest
    } = this.props;

    // Used the icon passed in and the player's info to build props for an icon for this event
    const { className: iconClass, style: iconStyle } = buildIconProps({
      ...icon,
      color: player
        ? rosterPalette.getRosterColorForPlayer(player.id)
        : 'white',
    });
    // Build an HTML element from the props
    const iconElement = Leaflet.divIcon({
      html: `<p class="${iconClass}" style="${reactToCss(iconStyle)}" />`,
    });

    return (
      <Marker
        position={toLeaflet(pos)}
        icon={iconElement}
        onClick={onMarkSelect}
        {...rest}
      >
        <MarkTooltip title={Localization.eventMarks[type].single} time={time}>
          {tooltip}
        </MarkTooltip>
      </Marker>
    );
  }
}

EventMark.propTypes = {
  type: PropTypes.string.isRequired,
  icon: PropTypes.objectOf(PropTypes.any).isRequired,
  pos: MapPropTypes.pos.isRequired,
  time: PropTypes.number.isRequired,
  tooltip: MapPropTypes.tooltipContent,
  player: PropTypes.shape({ id: PropTypes.string.isRequired }),
  rosterPalette: MapPropTypes.rosterPalette.isRequired,
  onMarkSelect: PropTypes.func,
};

EventMark.defaultProps = {
  player: null,
  tooltip: null,
  onMarkSelect: noop,
};

export default EventMark;
