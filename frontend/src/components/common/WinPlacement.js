import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';

import classes from './WinPlacement.module.scss';

const WinPlacement = ({ className, winPlace, rosterCount }) => (
  <div className={classNames(classes.container, className)}>
    <div className={classes.winPlace}>#{winPlace}</div>
    {rosterCount && <p className={classes.rosterCount}>/{rosterCount}</p>}
  </div>
);

WinPlacement.propTypes = {
  className: PropTypes.string,
  winPlace: PropTypes.number.isRequired,
  rosterCount: PropTypes.number,
};
WinPlacement.defaultProps = {
  className: null,
  rosterCount: null,
};

export default WinPlacement;
