import React from 'react';

import BtLogo from './BtLogo';
import PlayerSearch from './PlayerSearch';
import classes from './Header.module.scss';

const Header = () => (
  <div className={classes.header}>
    <BtLogo className={classes.logo} />
    <PlayerSearch />
    <a
      className={classes.gitlabIcon}
      href="https://gitlab.com/LucasPickering/battletrack"
    >
      <img
        src="https://gitlab.com/gitlab-com/gitlab-artwork/raw/master/logo/logo-square.svg"
        alt="GitLab"
      />
    </a>
  </div>
);

export default Header;
