import { DropdownButton, MenuItem } from 'react-bootstrap';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { noop } from 'lodash';
import PropTypes from 'prop-types';
import React, { useEffect } from 'react';
import classNames from 'classnames';

import { formatPlatform } from 'utils/formatters';
import actions from 'redux/actions';

import classes from './PlatformSelect.module.scss';

const PlatformSelect = ({
  className,
  activePlatform,
  onSelect,
  platforms,
  fetchPlatformsIfNeeded,
}) => {
  // Hook to fetch platforms from API. Will only be called on mount.
  useEffect(() => {
    fetchPlatformsIfNeeded();
  }, []);
  return (
    <div className={classNames(classes.platformSelect, className)}>
      <DropdownButton
        title={formatPlatform(activePlatform)}
        id="platforms-dropdown"
        onSelect={onSelect}
      >
        {platforms &&
          platforms.map(platform => (
            <MenuItem key={platform} eventKey={platform}>
              {formatPlatform(platform)}
            </MenuItem>
          ))}
      </DropdownButton>
    </div>
  );
};

PlatformSelect.propTypes = {
  className: PropTypes.string,
  activePlatform: PropTypes.string.isRequired,
  onSelect: PropTypes.func,

  // Redux state
  platforms: PropTypes.arrayOf(PropTypes.string.isRequired),

  // Redux dispatch
  fetchPlatformsIfNeeded: PropTypes.func.isRequired,
};

PlatformSelect.defaultProps = {
  className: null,
  platforms: [],
  onSelect: noop,
};

const mapStateToProps = state => ({
  platforms: state.api.platforms.data,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      fetchPlatformsIfNeeded: actions.api.platforms.requestIfNeeded,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PlatformSelect);
