import { FormControl } from 'react-bootstrap';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';

import { playerLink } from 'utils/links';
import Button from 'components/core/Button';
import Icon from 'components/core/Icon';

import PlatformSelect from './PlatformSelect';
import classes from './PlayerSearch.module.scss';

class PlayerSearch extends React.PureComponent {
  constructor(props, ...args) {
    super(props, ...args);
    this.state = {
      searchName: '',
      platform: props.defaultPlatform,
    };

    this.search = this.search.bind(this);
  }

  search() {
    const { history } = this.props;
    const { searchName, platform } = this.state;
    history.push(playerLink(platform, searchName));
  }

  render() {
    const { className } = this.props;
    const { searchName, platform } = this.state;

    return (
      <div className={classNames(classes.playerSearch, className)}>
        <PlatformSelect
          activePlatform={platform}
          onSelect={newPlatform => this.setState({ platform: newPlatform })}
        />
        <div className={classes.nameInputContainer}>
          <FormControl
            className={classes.nameInput}
            type="text"
            placeholder="Player name (case sensitive)"
            onChange={e => this.setState({ searchName: e.target.value })}
            onKeyPress={e => {
              if (e.key === 'Enter') {
                this.search();
              }
            }}
          />
          <Button
            background={false}
            disabled={!searchName}
            onClick={this.search}
          >
            <Icon name="search" />
          </Button>
        </div>
      </div>
    );
  }
}

PlayerSearch.propTypes = {
  className: PropTypes.string,
  defaultPlatform: PropTypes.string,
  history: PropTypes.objectOf(PropTypes.any).isRequired,
};

PlayerSearch.defaultProps = {
  className: null,
  defaultPlatform: 'steam', // PCMR
};

export default withRouter(PlayerSearch);
