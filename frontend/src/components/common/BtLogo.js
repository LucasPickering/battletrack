import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';

import classes from './BtLogo.module.scss';

const BtLogo = ({ className }) => (
  <Link className={classNames(classes.logo, className)} to="/">
    Battletrack
  </Link>
);

BtLogo.propTypes = {
  className: PropTypes.string,
};

BtLogo.defaultProps = {
  className: null,
};

export default BtLogo;
