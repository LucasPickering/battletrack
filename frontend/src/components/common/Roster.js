import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import React from 'react';

import { playerLink } from 'utils/links';
import DataPropTypes from 'proptypes/DataPropTypes';

import classes from './Roster.module.scss';

const Roster = ({ players, platform }) => (
  <ul className={classes.players}>
    {players.map(({ player_name: name, player_id: id }) => (
      <li key={id}>
        <Link to={playerLink(platform, name)}>{name}</Link>
      </li>
    ))}
  </ul>
);

Roster.propTypes = {
  players: PropTypes.arrayOf(DataPropTypes.playerSummary.isRequired).isRequired,
  platform: PropTypes.string.isRequired,
};

export default Roster;
