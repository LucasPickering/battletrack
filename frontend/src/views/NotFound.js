import React from 'react';

import classes from './NotFound.module.scss';

const NotFound = () => (
  <div className={classes.notFound}>
    <h1>Not Found</h1>
    <h3>No Bueno!</h3>
  </div>
);

export default NotFound;
