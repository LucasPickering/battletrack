import React from 'react';

import routeView from 'hoc/routeView';

import RecentMatches from './components/RecentMatches';
import classes from './HomeView.module.scss';

const HomeView = () => (
  <div className={classes.home}>
    <h1>Battletrack</h1>
    <div className={classes.recentMatches}>
      <h4 className={classes.recentMatchesLabel}>Recent Matches</h4>
      <RecentMatches />
    </div>
  </div>
);

export default routeView(HomeView);
