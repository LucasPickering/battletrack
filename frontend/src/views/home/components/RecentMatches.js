import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import React from 'react';

import {
  dateColumn,
  gameModeColumn,
  linkColumn,
  mapColumn,
  perspectiveColumn,
  platformColumn,
} from 'utils/table/columns';
import { matchLink } from 'utils/links';
import DataPropTypes from 'proptypes/DataPropTypes';
import DataTable from 'components/core/DataTable';
import apiConsumer, { RECENT_MATCHES_CONSUMER } from 'hoc/apiConsumer';
import apiFetcher, { RECENT_MATCHES_FETCHER } from 'hoc/apiFetcher';

const COLUMNS = [
  mapColumn({ accessor: 'map_name' }),
  gameModeColumn({ accessor: 'mode' }),
  perspectiveColumn({ accessor: 'perspective' }),
  platformColumn({ accessor: 'platform' }),
  dateColumn({
    accessor: 'date',
  }),
  linkColumn({
    accessor: 'id',
    linkBuilder: matchLink,
  }),
];

const RecentMatches = ({ matches }) => (
  <DataTable
    columns={COLUMNS}
    data={matches}
    defaultSorted={[{ id: 'date', desc: true }]}
    showPagination={false}
  />
);

RecentMatches.propTypes = {
  matches: PropTypes.arrayOf(DataPropTypes.matchSummary.isRequired).isRequired,
};

const mapStateToProps = state => ({
  matches: state.api.recentMatches.data,
});

export default apiFetcher(
  apiConsumer(connect(mapStateToProps)(RecentMatches), RECENT_MATCHES_CONSUMER),
  RECENT_MATCHES_FETCHER
);
