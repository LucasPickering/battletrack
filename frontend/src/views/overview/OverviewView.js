// Needs to be disabled because state is used for context
/* eslint-disable react/no-unused-state */
import { connect } from 'react-redux';
import { flatten } from 'lodash';
import PropTypes from 'prop-types';
import React from 'react';

import { EventMarkTypes, SpecialMarkTypes } from 'utils/MarkMappers';
import ApiPropTypes from 'proptypes/ApiPropTypes';
import OverviewContext from 'context/OverviewContext';
import RosterPalette from 'utils/RosterPalette';
import routeView from 'hoc/routeView';

import Overview from './components/Overview';

function makeInitialFilters(rosters) {
  return [
    ...Object.keys(SpecialMarkTypes), // plane, whiteZones, etc...
    ...Object.keys(EventMarkTypes), // Kill, Death, etc...
    // Every player ID, in a flat list
    ...flatten(
      rosters.map(roster => roster.players.map(player => player.player_id))
    ),
  ];
}

class OverviewView extends React.PureComponent {
  constructor(...args) {
    super(...args);

    this.state = {
      matchId: null,
      initialized: false,
      rosterPalette: null,
      enabledFilters: null,
      timeRange: null,
      setEnabledFilters: enabledFilters => this.setState({ enabledFilters }),
      setTimeRange: timeRange => this.setState({ timeRange }),
    };
  }

  componentDidMount() {
    this.initMatch();
  }

  componentDidUpdate() {
    this.initMatch();
  }

  /**
   * Initializes overview settings for this match
   */
  initMatch() {
    const {
      matchState: { data },
    } = this.props;

    if (data) {
      const { id, rosters } = data;
      const { matchId: oldMatchId } = this.state;
      if (id !== oldMatchId) {
        this.setState({
          matchId: id,
          initialized: true,
          rosterPalette: new RosterPalette(rosters),
          enabledFilters: makeInitialFilters(rosters),
          timeRange: [0, 0],
        });
      }
    }
  }

  render() {
    const { id } = this.props;

    return (
      <OverviewContext.Provider value={this.state}>
        <Overview urlParams={{ id }} />
      </OverviewContext.Provider>
    );
  }
}

OverviewView.propTypes = {
  // URL params
  id: PropTypes.string.isRequired, // Match ID

  // Redux state
  matchState: ApiPropTypes.apiState.isRequired,
};

const mapStateToProps = state => ({
  matchState: state.api.match,
  telemetryState: state.api.telemetry,
});

export default connect(mapStateToProps)(
  routeView(OverviewView, { showHeader: false })
);
