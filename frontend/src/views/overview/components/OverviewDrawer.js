import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import React, { useState } from 'react';

import { matchLink } from 'utils/links';
import Button from 'components/core/Button';
import Icon from 'components/core/Icon';

import OverviewFilterList from './OverviewFilterList';
import classes from './OverviewDrawer.module.scss';

const OverviewDrawer = ({ matchId }) => {
  // The hooks!
  const [drawerOpen, setDrawerOpen] = useState(true);

  return (
    <div className={classes.drawerContainer}>
      <Button
        className={classes.drawerButton}
        onClick={() => setDrawerOpen(!drawerOpen)}
      >
        <Icon name={drawerOpen ? 'chevron-left' : 'chevron-right'} />
      </Button>

      {drawerOpen && (
        <div className={classes.drawer}>
          <Link className={classes.matchLink} to={matchLink(matchId)}>
            <Icon name="arrow-left" /> Back To Match
          </Link>
          <OverviewFilterList />
        </div>
      )}
    </div>
  );
};

OverviewDrawer.propTypes = {
  // Redux state
  matchId: PropTypes.string.isRequired,
};

const mapStateToProps = state => ({
  matchId: state.api.match.data.id,
});

export default connect(mapStateToProps)(OverviewDrawer);
