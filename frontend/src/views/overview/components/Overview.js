import React, { useContext } from 'react';

import apiConsumer, { MATCH_CONSUMER } from 'hoc/apiConsumer';
import apiFetcher, { MATCH_FETCHER, TELEMETRY_FETCHER } from 'hoc/apiFetcher';

import OverviewContext from 'context/OverviewContext';
import OverviewDrawer from './OverviewDrawer';
import OverviewMap from './OverviewMap';
import OverviewTimeRange from './OverviewTimeRange';
import classes from './Overview.module.scss';

const Overview = () => {
  const { initialized } = useContext(OverviewContext);
  return (
    initialized && (
      <div className={classes.container}>
        <OverviewDrawer />
        <div className={classes.mapContainer}>
          <OverviewTimeRange />
          <OverviewMap />
        </div>
      </div>
    )
  );
};

export default apiFetcher(
  apiConsumer(Overview, MATCH_CONSUMER),
  MATCH_FETCHER,
  TELEMETRY_FETCHER
);
