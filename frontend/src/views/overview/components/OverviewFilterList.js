import { connect } from 'react-redux';
import { max } from 'lodash';
import CheckboxTree from 'react-checkbox-tree';
import PropTypes from 'prop-types';
import React, { useContext, useMemo, useState } from 'react';

import { EventMarkTypes, SpecialMarkTypes } from 'utils/MarkMappers';
import Icon from 'components/core/Icon';
import Localization from 'utils/Localization';
import OverviewContext from 'context/OverviewContext';

import classes from './OverviewFilterList.module.scss';

import 'react-checkbox-tree/lib/react-checkbox-tree.css';

const ROSTER_ICON = 'users';
const PLAYER_ICON = 'user';

/**
 * An object of all nodes to display in the checkbox tree.
 * @param {object[]} rosters All rosters in the game
 * @param {object} rosterPalette Color palette for each roster/player
 * @returns {object} All checkbox nodes
 */
function getNodes(rosters, rosterPalette) {
  // Special filters (plane, zones, etc.)
  const specialFilterNodes = Object.entries(SpecialMarkTypes).map(
    ([
      key,
      { icon }, // Props for the Icon component
    ]) => ({
      value: key,
      label: Localization.specialMarks[key],
      icon: <Icon {...icon} />,
    })
  );

  // Event type filters
  const eventFilterNodes = Object.entries(EventMarkTypes).map(
    ([
      key,
      { icon }, // Props for the Icon component
    ]) => ({
      value: key,
      label: Localization.eventMarks[key].plural,
      icon: <Icon {...icon} />,
    })
  );

  // Roster nodes
  let rosterNodes;
  // Check if this is a solo game - this will affect how we show the list
  if (max(rosters.map(r => r.players.length)) > 1) {
    // More than one player per roster - build one node per roster and one
    // sub-node per player
    rosterNodes = rosters.map(({ id, win_place: winPlace, players }) => ({
      value: id,
      label: `#${winPlace}`,
      icon: (
        <Icon name={ROSTER_ICON} color={rosterPalette.getRosterColor(id)} />
      ),
      // One child node per player
      children: players.map(
        ({ player_id: playerId, player_name: playerName }) => ({
          value: playerId,
          label: playerName,
          icon: (
            <Icon
              name={PLAYER_ICON}
              color={rosterPalette.getPlayerColor(playerId)}
            />
          ),
        })
      ),
    }));
  } else {
    // There is only one player per roster, so don't nest them.
    rosterNodes = rosters.map(
      ({
        win_place: winPlace,
        // players is an array of 1, so unpack it to ID/name
        players: [{ player_id: playerId, player_name: playerName }],
      }) => ({
        value: playerId,
        label: `#${winPlace} - ${playerName}`,
        icon: (
          <Icon
            name={PLAYER_ICON}
            color={rosterPalette.getRosterColorForPlayer(playerId)}
          />
        ),
      })
    );
  }

  return [
    {
      value: 'filters',
      label: 'Filters',
      icon: <Icon name="filter" />,
      children: specialFilterNodes.concat(eventFilterNodes),
    },
    {
      value: 'rosters',
      label: 'Rosters',
      icon: <Icon name={ROSTER_ICON} />,
      children: rosterNodes,
    },
  ];
}

const OverviewFilterList = ({ rosters }) => {
  // Hooks!
  const { rosterPalette, enabledFilters, setEnabledFilters } = useContext(
    OverviewContext
  );
  // Array of expanded nodes
  const [expanded, setExpanded] = useState(['filters', 'rosters']);
  // Memoize the function to build nodes. Only build new nodes when the
  // rosters change.
  const nodes = useMemo(() => getNodes(rosters, rosterPalette), [rosters]);

  return (
    <div className={classes.overviewFilterList}>
      <CheckboxTree
        nodes={nodes}
        checked={enabledFilters}
        expanded={expanded}
        onCheck={setEnabledFilters}
        onExpand={setExpanded}
      />
    </div>
  );
};

OverviewFilterList.propTypes = {
  // Redux state
  rosters: PropTypes.arrayOf(PropTypes.object).isRequired,
};

const mapStateToProps = state => ({
  rosters: state.api.match.data.rosters,
});

export default connect(mapStateToProps)(OverviewFilterList);
