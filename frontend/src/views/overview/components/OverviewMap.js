import { connect } from 'react-redux';
import { flatten, pick } from 'lodash';
import React, { useContext, useMemo } from 'react';

import { EventTypes, convertEvent } from 'utils/MarkMappers';
import { inRangeIncl } from 'utils/funcs';
import DataPropTypes from 'proptypes/DataPropTypes';
import MarkedGameMap from 'components/map/MarkedGameMap';
import OverviewContext from 'context/OverviewContext';
import apiConsumer, { TELEMETRY_CONSUMER } from 'hoc/apiConsumer';

import './OverviewMap.module.scss';

function getEventMarks(events) {
  // Convert events to marks. Each event can become one or more mark. Events look like:
  // {
  //   EventType1: [e1, e2, ...],
  //   ...
  // }
  // Marks will be an object of MarkType:list, containing all marks that might be displayed.
  // {
  //   MarkType1: [m1, m2, ...],
  //   ...
  // }
  // Filtering on time/type/player is based on state so that will be done during render.
  const eventMarks = Object.entries(EventTypes).reduce(
    (acc, [eventType, markTypes]) => {
      const eventsForType = events[eventType];
      markTypes.forEach(markType => {
        acc[markType] = eventsForType
          .map(event => convertEvent(markType, event)) // Convert the event to a mark
          .filter(mark => mark); // Filter out null marks
      });
      return acc;
    },
    {}
  );
  Object.freeze(eventMarks);
  return eventMarks;
}

const OverviewMap = ({
  map,
  telemetry: { plane, zones: whiteZones, events },
}) => {
  const {
    rosterPalette,
    enabledFilters,
    timeRange: [minTime, maxTime],
  } = useContext(OverviewContext);
  const allEventMarks = useMemo(() => getEventMarks(events), [events]);

  // Build an object of special marks to display,
  // but filter out ones that are disabled
  const specialMarks = pick({ plane, whiteZones }, enabledFilters); // Filter by type

  // Filter marks by type/time/player and flatten them into one big list
  const filteredByType = pick(allEventMarks, enabledFilters); // Filter by type
  const eventMarks = flatten(
    Object.values(filteredByType)
      // Filter each mark list by time/player
      .map(markList =>
        markList
          .filter(({ time }) => inRangeIncl(time, minTime, maxTime)) // Filter by time
          .filter(({ player }) => !player || enabledFilters.includes(player.id))
      )
  ); // And by player

  return (
    <MarkedGameMap
      map={map}
      specialMarks={specialMarks}
      eventMarks={eventMarks}
      rosterPalette={rosterPalette}
      showGrid
    />
  );
};

OverviewMap.propTypes = {
  // Redux state
  map: DataPropTypes.map.isRequired,
  telemetry: DataPropTypes.telemetry.isRequired,
};

const mapStateToProps = state => ({
  map: state.api.match.data.map,
  telemetry: state.api.telemetry.data,
});

export default apiConsumer(
  connect(mapStateToProps)(OverviewMap),
  TELEMETRY_CONSUMER
);
