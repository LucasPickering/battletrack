import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import React from 'react';

import OverviewContext from 'context/OverviewContext';
import TimeRange from 'components/map/TimeRange';

const OverviewTimeRange = ({ matchDuration }) => (
  <OverviewContext.Consumer>
    {({ timeRange, setTimeRange }) => (
      <TimeRange
        // Sometimes the final event(s) of a match can take place after the
        // duration, e.g. the duration is 1906 but the final kill occurs
        // at 1906.1. Add one to make sure we capture every event.
        maxTime={matchDuration + 1}
        timeRange={timeRange}
        setTimeRange={setTimeRange}
      />
    )}
  </OverviewContext.Consumer>
);

OverviewTimeRange.propTypes = {
  matchDuration: PropTypes.number.isRequired,
};

const mapStateToProps = state => ({
  matchDuration: state.api.match.data.duration,
});

export default connect(mapStateToProps)(OverviewTimeRange);
