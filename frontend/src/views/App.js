import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import React from 'react';

import HomeView from './home/HomeView';
import MatchView from './match/MatchView';
import NotFound from './NotFound';
import OverviewView from './overview/OverviewView';
import PlayerView from './player/PlayerView';

import 'styles/bootstrap/Button.scss';
import 'styles/bootstrap/Dropdown.scss';
import 'styles/bootstrap/FormControl.scss';
import 'styles/bootstrap/Panel.scss';
import 'styles/colors.scss';

import './App.scss';

const App = () => (
  <Router>
    <Switch>
      <Route exact path="/" component={HomeView} />
      <Route exact path="/players/:platform/:name" component={PlayerView} />
      <Route exact path="/matches/:id" component={MatchView} />
      <Route exact path="/matches/:id/overview" component={OverviewView} />
      <Route component={NotFound} />
    </Switch>
  </Router>
);

export default App;
