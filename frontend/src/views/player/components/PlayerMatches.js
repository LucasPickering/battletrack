import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import React, { useMemo } from 'react';

import {
  dateColumn,
  gameModeColumn,
  linkColumn,
  mapColumn,
  perspectiveColumn,
  winPlaceColumn,
} from 'utils/table/columns';
import { matchLink } from 'utils/links';
import DataPropTypes from 'proptypes/DataPropTypes';
import DataTable from 'components/core/DataTable';
import Roster from 'components/common/Roster';
import apiConsumer, { PLAYER_CONSUMER } from 'hoc/apiConsumer';
import apiFetcher, { PLAYER_FETCHER } from 'hoc/apiFetcher';
import makeCellComponent from 'hoc/makeCellComponent';

const makeColumns = ({ playerId }) => [
  winPlaceColumn({
    accessor: 'stats.win_place',
    rosterCountGetter: original => original.summary.roster_count,
  }),
  mapColumn({ accessor: 'summary.map_name' }),
  gameModeColumn({ accessor: 'summary.mode' }),
  perspectiveColumn({ accessor: 'summary.perspective' }),
  dateColumn({ accessor: 'summary.date' }),
  {
    accessor: 'roster',
    filterable: false,
    sortable: false,
    Header: 'Teammates',
    Cell: makeCellComponent(Roster, ({ value, original }) => ({
      players: value.filter(player => player.player_id !== playerId),
      platform: original.summary.platform,
    })),
  },
  linkColumn({
    accessor: 'match_id',
    linkBuilder: matchLink,
  }),
];

const PlayerMatches = ({ className, player: { id, matches } }) => {
  const columns = useMemo(() => makeColumns({ playerId: id }), [id]);
  return (
    <DataTable
      className={className}
      columns={columns}
      data={matches}
      defaultSorted={[{ id: 'summary.date', desc: true }]}
      filterable
    />
  );
};

PlayerMatches.propTypes = {
  className: PropTypes.string,
  player: DataPropTypes.player.isRequired,
};

PlayerMatches.defaultProps = {
  className: null,
};

const mapStateToProps = state => ({
  player: state.api.player.data,
});

export default apiFetcher(
  apiConsumer(connect(mapStateToProps)(PlayerMatches), PLAYER_CONSUMER),
  PLAYER_FETCHER
);
