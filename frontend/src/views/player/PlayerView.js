import PropTypes from 'prop-types';
import React from 'react';

import routeView from 'hoc/routeView';

import PlayerMatches from './components/PlayerMatches';
import classes from './PlayerView.module.scss';

const PlayerView = ({ platform, name }) => (
  <div className={classes.player}>
    <h2>{name}</h2>
    <PlayerMatches className={classes.table} urlParams={{ platform, name }} />
  </div>
);

PlayerView.propTypes = {
  // URL params
  platform: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
};

export default routeView(PlayerView);
