import { Link } from 'react-router-dom';
import { Panel } from 'react-bootstrap';
import PropTypes from 'prop-types';
import React from 'react';

import { formatSeconds } from 'utils/formatters';
import { playerLink } from 'utils/links';
import DataPropTypes from 'proptypes/DataPropTypes';

import classes from './RosterMatchSummary.module.scss';

const RosterMatchSummary = props => {
  const { platform, data, ...rest } = props;
  const { win_place: winPlace, players } = data;

  return (
    <Panel className={classes.rosterMatchSummary} {...rest}>
      <p className={classes.placement}>#{winPlace}</p>
      <table className={classes.rosterTable}>
        <thead>
          <tr>
            <th style={{ width: 180 }} />
            <th>Kills</th>
            <th>Time Alive</th>
          </tr>
        </thead>
        <tbody>
          {players.map(({ player_id: id, player_name: name, stats }) => (
            <tr key={id}>
              <td>
                <Link to={playerLink(platform, name)}>{name}</Link>
              </td>
              <td>{stats.kills}</td>
              <td>{formatSeconds(stats.time_survived)}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </Panel>
  );
};

RosterMatchSummary.propTypes = {
  platform: PropTypes.string.isRequired,
  data: DataPropTypes.rosterMatch.isRequired,
};

export default RosterMatchSummary;
