import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import {
  formatDate,
  formatMapName,
  formatPlaylist,
  formatSeconds,
} from 'utils/formatters';
import { overviewLink } from 'utils/links';
import { winPlaceColumn } from 'utils/table/columns';
import Button from 'components/core/Button';
import DataPropTypes from 'proptypes/DataPropTypes';
import DataTable from 'components/core/DataTable';
import React, { useMemo } from 'react';
import Roster from 'components/common/Roster';
import apiConsumer, { MATCH_CONSUMER } from 'hoc/apiConsumer';
import apiFetcher, { MATCH_FETCHER } from 'hoc/apiFetcher';
import makeCellComponent from 'hoc/makeCellComponent';

import classes from './Match.module.scss';

const makeColumns = ({ platform }) => [
  winPlaceColumn({ accessor: 'win_place', sortable: false, width: 64 }),
  {
    accessor: 'players',
    sortable: false,
    Header: 'Players',
    Cell: makeCellComponent(Roster, ({ value }) => ({
      players: value,
      platform,
    })),
  },
  {
    accessor: 'players',
    sortable: false,
    Header: 'Kills',
    // TODO make this good
    Cell: ({ value }) =>
      value.map(player => <p key={player.player_id}>{player.stats.kills}</p>),
  },
  {
    accessor: 'players',
    sortable: false,
    Header: 'Time Alive',
    // TODO make this good
    Cell: ({ value }) =>
      value.map(player => (
        <p key={player.player_id}>
          {formatSeconds(player.stats.time_survived)}
        </p>
      )),
  },
];

const Match = ({
  match: {
    id,
    platform,
    mode,
    perspective,
    map: { name: mapName },
    date,
    duration,
    rosters,
  },
}) => {
  const columns = useMemo(() => makeColumns({ platform }), [platform]);
  return (
    <div className={classes.match}>
      <div className={classes.matchInfo}>
        <h2>{formatPlaylist(mode, perspective)}</h2>
        <h2 className={classes.right}>{formatMapName(mapName)}</h2>
        <h3>{formatDate(date)}</h3>
        <h3 className={classes.right}>{formatSeconds(duration)}</h3>
      </div>

      <Link to={overviewLink(id)}>
        <Button>Overview</Button>
      </Link>

      <DataTable
        className={classes.table}
        columns={columns}
        data={rosters}
        defaultSorted={[{ id: 'win_place' }]}
        defaultPageSize={rosters.length}
        showPagination={false}
      />
    </div>
  );
};

Match.propTypes = {
  match: DataPropTypes.match.isRequired,
};

const mapStateToProps = state => ({
  match: state.api.match.data,
});

export default apiFetcher(
  apiConsumer(connect(mapStateToProps)(Match), MATCH_CONSUMER),
  MATCH_FETCHER
);
