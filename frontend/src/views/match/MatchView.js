import PropTypes from 'prop-types';
import React from 'react';

import routeView from 'hoc/routeView';

import Match from './components/Match';

const MatchView = ({ id }) => <Match urlParams={{ id }} />;

MatchView.propTypes = {
  id: PropTypes.string.isRequired,
};

export default routeView(MatchView);
