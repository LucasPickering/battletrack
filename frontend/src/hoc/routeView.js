import Header from 'components/common/Header';
import React from 'react';
import classNames from 'classnames';

import classes from './routeView.module.scss';

/**
 * Wraps the given component so that it takes props from the URL and renders as
 * a fullscreen page.
 *
 * @param {ReactComponent} Component Component to wrap
 * @param {bool} opts.showHeader Should this component show the page header?
 */
export default (Component, { showHeader = true } = {}) => ({
  match,
  ...rest
}) => (
  <div className={classNames(classes.pageContainer, 'min-full-size')}>
    {showHeader && <Header />}
    <div
      className={classNames(classes.childContainer, {
        [classes.withHeader]: showHeader,
      })}
    >
      <Component {...match.params} {...rest} />
    </div>
  </div>
);
