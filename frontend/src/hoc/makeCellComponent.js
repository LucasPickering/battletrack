import React from 'react';

export default function(Component, propMapper) {
  return props => <Component {...propMapper(props)} />;
}
