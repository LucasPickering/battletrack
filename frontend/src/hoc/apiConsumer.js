import { connect } from 'react-redux';
import React from 'react';

import { getHocDisplayName } from 'utils/funcs';
import ApiError from 'components/core/ApiError';
import ApiPropTypes from 'proptypes/ApiPropTypes';
import Loading from 'components/core/Loading';

export const RECENT_MATCHES_CONSUMER = {
  reduxStateKey: 'recentMatches',
  loadingText: 'Loading matches...',
};

export const PLAYER_CONSUMER = {
  reduxStateKey: 'player',
  loadingText: 'Loading matches...',
};
export const MATCH_CONSUMER = {
  reduxStateKey: 'match',
  loadingText: 'Loading match...',
};

export const TELEMETRY_CONSUMER = {
  reduxStateKey: 'telemetry',
  loadingText: 'Loading match data...',
};

export default (Component, { reduxStateKey, loadingText = null } = {}) => {
  const ApiConsumer = ({ apiState, ...rest }) => {
    // If loading, display a loading status
    if (apiState.loading) {
      return <Loading text={loadingText} />;
    }

    // If is an error, render it
    if (apiState.error) {
      return <ApiError error={apiState.error} />;
    }

    // If we have data, render the component
    if (apiState.data) {
      return <Component {...rest} />;
    }

    // No data, no error, no loading - should only get here on first render
    return null;
  };

  ApiConsumer.displayName = getHocDisplayName('ApiConsumer', Component);

  ApiConsumer.propTypes = {
    apiState: ApiPropTypes.apiState.isRequired, // From Redux state
  };

  const mapStateToProps = state => ({
    apiState: state.api[reduxStateKey],
  });

  return connect(mapStateToProps)(ApiConsumer);
};
