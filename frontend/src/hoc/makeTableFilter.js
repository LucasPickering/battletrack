import { get } from 'lodash';
import React from 'react';

export default function(Component, props) {
  return ({ filter, onChange }) => (
    <Component selected={get(filter, 'value')} onChange={onChange} {...props} />
  );
}
