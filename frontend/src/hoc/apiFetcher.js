import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { keyBy, mapValues, omit } from 'lodash';
import PropTypes from 'prop-types';
import React, { useEffect } from 'react';

import { getHocDisplayName } from 'utils/funcs';
import actions from 'redux/actions';

export const RECENT_MATCHES_FETCHER = {
  name: 'recentMatches',
  requestAction: actions.api.recentMatches.requestIfNeeded,
  urlParamNames: [],
};

export const PLAYER_FETCHER = {
  name: 'player',
  requestAction: actions.api.player.requestIfNeeded,
  urlParamNames: ['platform', 'name'],
};
export const MATCH_FETCHER = {
  name: 'match',
  requestAction: actions.api.match.requestIfNeeded,
  urlParamNames: ['id'],
};

export const TELEMETRY_FETCHER = {
  name: 'telemetry',
  requestAction: actions.api.telemetry.requestIfNeeded,
  urlParamNames: ['id'],
};

/**
 * Wraps the given component in 1+ components that each send a request for one
 * resource. Each request config passed in will become one wrapping component.
 */
export default (Component, ...requestConfigs) => {
  const withRequesterName = requestConfigs.map(({ name, ...rest }) => ({
    requesterName: `${name}_requestIfNeeded`,
    ...rest,
  }));
  const requesterNames = requestConfigs.map(
    ({ requesterName }) => requesterName
  );
  const byRequesterName = keyBy(withRequesterName, 'requesterName');

  const ApiFetcher = ({ urlParams, ...rest }) => {
    withRequesterName.forEach(({ requesterName, urlParamNames }) => {
      const urlParamArray = urlParamNames.map(pName => urlParams[pName]);
      useEffect(() => {
        rest[requesterName](urlParams);
      }, urlParamArray);
    });

    const subcomponentProps = omit(rest, requesterNames);

    return <Component {...subcomponentProps} />;
  };

  ApiFetcher.displayName = getHocDisplayName('ApiFetcher', Component);

  ApiFetcher.propTypes = {
    urlParams: PropTypes.shape(), // From parent
    // From Redux dispatch
    ...mapValues(byRequesterName, () => PropTypes.func.isRequired),
  };

  ApiFetcher.defaultProps = {
    urlParams: null,
  };

  const mapDispatchToProps = dispatch =>
    bindActionCreators(
      mapValues(byRequesterName, ({ requestAction }) => requestAction),
      dispatch
    );

  return connect(
    null,
    mapDispatchToProps
  )(ApiFetcher);
};
