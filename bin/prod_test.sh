#!/bin/sh

# Script to run a local version of the production setup
DB_DIR=$PWD/.database
LOG_DIR=$PWD/.logs
mkdir -p $DB_DIR
mkdir -p $LOG_DIR

export AMPLIFY_API_KEY=$(cat ./keys/amplify)
export POSTGRES_DATA_DIR=$DB_DIR
export BT_LOGGING_DIR=$LOG_DIR
export BT_DEV_API_KEY=$(cat ./keys/pubg)
export BT_DB_PASSWORD=btpass
export BT_SECRET_KEY=secret
export CI_COMMIT_REF_SLUG=latest

IMAGE_REGISTRY=registry.gitlab.com/lucaspickering/battletrack
BACKEND_IMAGE=${IMAGE_REGISTRY}/backend:${CI_COMMIT_REF_SLUG}
FRONTEND_IMAGE=${IMAGE_REGISTRY}/frontend:${CI_COMMIT_REF_SLUG}
PROD_IMAGE=${IMAGE_REGISTRY}/prod:${CI_COMMIT_REF_SLUG}
NGINX_AMPLIFY_IMAGE=${IMAGE_REGISTRY}/nginx-amplify:latest

docker build -t $BACKEND_IMAGE backend/
docker build -t $FRONTEND_IMAGE frontend/
docker build --pull -t ${NGINX_AMPLIFY_IMAGE} https://github.com/nginxinc/docker-nginx-amplify.git
docker build -t ${PROD_IMAGE} --build-arg tag=${CI_COMMIT_REF_SLUG} prod/
docker-compose -f docker-compose.prod.yml up
